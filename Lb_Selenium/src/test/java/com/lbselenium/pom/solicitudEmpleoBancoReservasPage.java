package com.lbselenium.pom;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.util.ArrayList;

public class solicitudEmpleoBancoReservasPage {

    //WebElements declarados
    @FindBy(xpath = "//a[@class='btn-more btn btn-transparent orange']")
    private WebElement btnParteNosotros;
    @FindBy(id = "nombre")
    private WebElement nombre;
    @FindBy(id = "apellidos")
    private WebElement apellido;
    @FindBy(id = "fecha_nac")
    private WebElement fecha_nac;
    @FindBy(id = "sexo")
    private WebElement dropSexo;
    @FindBy(id = "id_tipo_dni")
    private WebElement documentoIdentificativo;
    @FindBy(id = "dni")
    private WebElement identificativoPersonal;
    @FindBy(id = "id_estado_civil")
    private WebElement estadoCivil;
    @FindBy(id = "email_part")
    private WebElement inputEmail;
    @FindBy(id = "telefono_part")
    private WebElement telefResidencial;
    @FindBy(id = "movil_part")
    private WebElement movil;
    @FindBy(id = "direccion")
    private WebElement direccion;
    @FindBy(id = "poblacion")
    private WebElement Poblacion;

    //declaricon variabes
    private String Nombre = "VICTOR " + " MANUEL";
    private String Apellido = "ALCANTARA " + " FRIAS ";
    private String FechaNac = "02/02/1981";
    private String Sexo = "M";
    private String identificativo = "006";
    private String identificacionPersonal = "00115530967";
    private String estado_civil = "002";
    private String correoElectronico = "victorfrias2000@hotmail.com";
    private String residencialTelefono = "8093330909";
    private String Movil = "8293339898";
    private String Direccion = "las palmas herreras santo domingo norte";
    private String ciudad = "SANTO DOMINGO ESTE";

    //declaracion de variables
    private WebDriver driver;

    //constructor de la clase
    public solicitudEmpleoBancoReservasPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void accesoPortalSolicitudEmpleo() throws InterruptedException {

        btnParteNosotros.click();
        Thread.sleep(5000);
        //para moverme de una pestana a otra en el navegador chrome
        ArrayList<String> pestana = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(pestana.get(1));
        nombre.sendKeys(Nombre);
        apellido.sendKeys(Apellido);
        fecha_nac.sendKeys(FechaNac);
        selectSexo(dropSexo);
        selectDocumentIdentificativo(documentoIdentificativo);
        identificativoPersonal.sendKeys(identificacionPersonal);
        selectEstadoCivil(estadoCivil);
        inputEmail.sendKeys(correoElectronico);
        telefResidencial.sendKeys(residencialTelefono);
        movil.sendKeys(Movil);
        direccion.sendKeys(Direccion);
        Poblacion.sendKeys(ciudad);

    }

    public void selectSexo(WebElement locator) {
        Select selct = new Select(locator);
        selct.selectByValue(Sexo);
    }

    public void selectDocumentIdentificativo(WebElement locator) {
        Select selct = new Select(locator);
        selct.selectByValue(identificativo);
    }

    public void selectEstadoCivil(WebElement locator) {
        Select selct = new Select(locator);
        selct.selectByValue(estado_civil);
    }


}
